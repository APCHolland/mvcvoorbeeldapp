﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testApplicatie.Models
{
    public class TestViewModel
    {
        public int Getal { get; set; }
        public string Tekst { get; set; }
        public DateTime DateNow { get; set; }
    }
}
