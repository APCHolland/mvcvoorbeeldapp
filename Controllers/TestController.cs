﻿using Microsoft.AspNetCore.Mvc;
using System;
using testApplicatie.Models;

namespace testApplicatie.Controllers
{
    public class TestController : Controller
    {
        [HttpGet]
        public IActionResult Index(int waarde)
        {
            TestViewModel model;
            switch (waarde)
            {
                case 1:
                    model = new TestViewModel
                    {
                        Getal = 1,
                        Tekst = "Eén",
                        DateNow = DateTime.Now
                    };
                    break;
                case 2:
                    model = new TestViewModel
                    {
                        Getal = 2,
                        Tekst = "Twee",
                        DateNow = DateTime.Now
                    };
                    break;
                default:
                    model = new TestViewModel
                    {
                        Getal = 99999,
                        Tekst = "default",
                        DateNow = DateTime.Now
                    };
                    break;
            }

            return View(model);
        }

        [HttpPost]
        public void ReturnCall(string waarde)
        {
            Console.WriteLine(waarde);
        }
    }
}
